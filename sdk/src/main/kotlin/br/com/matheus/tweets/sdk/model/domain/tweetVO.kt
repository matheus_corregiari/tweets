package br.com.matheus.tweets.sdk.model.domain

import android.os.Parcel
import br.com.matheus.tweets.sdk.extension.KParcelable
import br.com.matheus.tweets.sdk.extension.parcelableCreator
import br.com.matheus.tweets.sdk.extension.readTypedObjectCompat
import br.com.matheus.tweets.sdk.extension.writeTypedObjectCompat
import com.google.gson.annotations.Expose

data class TweetVO(
        @Expose val id: Long,
        @Expose val createdAt: String,
        @Expose val text: String,
        @Expose val user: UserVO
) : KParcelable {

    companion object {
        @JvmField
        val CREATOR = parcelableCreator(::TweetVO)
    }

    private constructor(source: Parcel) : this(
            id = source.readLong(),
            createdAt = source.readString(),
            text = source.readString(),
            user = source.readTypedObjectCompat(UserVO.CREATOR)!!
    )

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeLong(id)
        writeString(createdAt)
        writeString(text)
        writeTypedObjectCompat(user, flags)
    }
}

internal class NewTweetVO(@Expose val status: String) : KParcelable {

    companion object {
        val CREATOR = parcelableCreator(::NewTweetVO)
    }

    private constructor(source: Parcel) : this(status = source.readString())

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(status)
    }
}