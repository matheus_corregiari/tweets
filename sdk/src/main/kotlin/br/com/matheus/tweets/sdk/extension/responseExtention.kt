@file:JvmName("ResponseUtils")

package br.com.matheus.tweets.sdk.extension

import br.com.matheus.tweets.sdk.model.DataResult
import br.com.matheus.tweets.sdk.model.type.DataResultStatus
import br.com.matheus.tweets.sdk.model.type.ERROR
import br.com.matheus.tweets.sdk.model.type.LOADING

internal fun loadingResponse() = DataResult(null, null, LOADING)

internal fun <T> T?.toDataResponse(@DataResultStatus status: Long) = DataResult(this, null, status)

internal fun <T> T?.toDataResponseWithError(error: Throwable) = DataResult(this, error, ERROR)

internal fun <T> Throwable.toErrorResponse() = DataResult<T>(null, this, ERROR)