package br.com.matheus.tweets.sdk.model.domain

import android.os.Parcel
import br.com.matheus.tweets.sdk.extension.KParcelable
import br.com.matheus.tweets.sdk.extension.parcelableCreator
import com.google.gson.annotations.Expose

internal data class SearchResultVO(
        @Expose val statuses: List<TweetVO> = emptyList()
) : KParcelable {

    companion object {
        @JvmField
        val CREATOR = parcelableCreator(::SearchResultVO)
    }

    private constructor(source: Parcel) : this(
            statuses = source.createTypedArrayList(TweetVO.CREATOR)
    )

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeTypedList(statuses)
    }
}