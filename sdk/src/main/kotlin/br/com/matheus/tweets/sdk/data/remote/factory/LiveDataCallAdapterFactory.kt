package br.com.matheus.tweets.sdk.data.remote.factory

import br.com.matheus.tweets.sdk.data.ResponseLiveData
import br.com.matheus.tweets.sdk.extension.loadingResponse
import br.com.matheus.tweets.sdk.extension.toDataResponse
import br.com.matheus.tweets.sdk.extension.toErrorResponse
import br.com.matheus.tweets.sdk.model.DataResult
import br.com.matheus.tweets.sdk.model.type.SUCCESS
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.Exception
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

internal class LiveDataCallAdapterFactory : CallAdapter.Factory() {

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        if (getRawType(returnType) != ResponseLiveData::class.java) return null

        val type = getParameterUpperBound(0, returnType as ParameterizedType)
        return LiveDataCallAdapter<Any>(type)
    }
}

internal class LiveDataCallAdapter<RESULT>(private val responseType: Type) : CallAdapter<RESULT, ResponseLiveData<RESULT>> {

    override fun responseType() = responseType

    override fun adapt(call: Call<RESULT>) = object : ResponseLiveData<RESULT>() {
        override fun compute() {
            value = loadingResponse()
            makeRequest(this::postValue, call)
        }
    }

    private fun makeRequest(value: (DataResult<RESULT>) -> Unit, request: Call<RESULT>) {
        Thread(Runnable {
            value(try {
                val response = if (request.isExecuted) request.clone().execute() else request.execute()
                val data = response.body()

                data.toDataResponse(SUCCESS)
            } catch (error: Exception) {
                error.toErrorResponse<RESULT>()
            })
        }).start()
    }

}