package br.com.matheus.tweets.sdk.model

import android.os.Parcel
import br.com.matheus.tweets.sdk.data.remote.adapter.DateTypeAdapter
import br.com.matheus.tweets.sdk.extension.KParcelable
import br.com.matheus.tweets.sdk.extension.parcelableCreator
import com.google.gson.annotations.Expose
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeParseException
import timber.log.Timber
import java.util.*

data class DateType(@Expose private val localDateTime: LocalDateTime = LocalDateTime.now()) : KParcelable {

    constructor(localDate: LocalDate) : this(
            LocalDateTime.from(localDate.atStartOfDay(ZoneOffset.UTC))
    )

    private constructor(source: Parcel) : this(source.readSerializable() as LocalDateTime)

    val fullDateName get() = localDateTime.toLocalDate().format(DateTimeFormatter.ofPattern("dd MMM yyyy", Locale("pt", "BR")))

    val apiFormat: String
        get() = ZonedDateTime.of(localDateTime, ZoneOffset.UTC).format(DateTypeAdapter.API_FORMAT)

    val date get() = localDateTime.toLocalDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))

    val dateWithDay: String
        get() {
            val dayWithDate = localDateTime.format(
                    DateTimeFormatter.ofPattern("EEEE, dd/MM/yyyy", Locale("pt", "BR"))).replace("-feira", "")
            return String.format("%s%s", dayWithDate.substring(0, 1).toUpperCase(), dayWithDate.substring(1))
        }

    val dateWithFullWeekDay: String
        get() {
            val dayWithDate = localDateTime.format(
                    DateTimeFormatter.ofPattern("EEEE, dd/MM/yyyy", Locale("pt", "BR")))
            return String.format("%s%s", dayWithDate.substring(0, 1).toUpperCase(), dayWithDate.substring(1))
        }

    val dateWithTime: String
        get() {
            val dayWithDate = localDateTime.format(
                    DateTimeFormatter.ofPattern("EEE, dd/MM/yyyy hh'h'mm", Locale("pt", "BR"))).replace("-feira", "")
            return String.format("%s%s", dayWithDate.substring(0, 1).toUpperCase(), dayWithDate.substring(1))
        }

    val time get() = localDateTime.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm"))

    val dateTime get() = localDateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"))

    val headerTime: String
        get() {
            val header = localDateTime.format(
                    DateTimeFormatter.ofPattern("MMMM / yyyy", Locale("pt", "BR"))).toLowerCase()
            return String.format("%s%s", header.substring(0, 1).toUpperCase(), header.substring(1))
        }

    val dateInMillis get() = localDateTime.atZone(ZoneOffset.UTC).toInstant().toEpochMilli()

    fun validateGreaterThan18(): Boolean {
        return localDateTime.plusYears(18).isBefore(LocalDateTime.now())
    }

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeSerializable(localDateTime)
    }

    companion object {

        val CREATOR = parcelableCreator(::DateType)

        fun fromDateLayout(date: String): DateType? {
            return try {
                DateType(
                        LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy")).atTime(0, 0, 1))
            } catch (e: DateTimeParseException) {
                Timber.e(e)
                null
            }

        }

        fun fromDateAPIFormat(date: String): DateType? {
            return try {
                DateType(
                        LocalDate.parse(date, DateTypeAdapter.API_FORMAT))
            } catch (e: DateTimeParseException) {
                Timber.e(e)
                null
            }
        }
    }
}
