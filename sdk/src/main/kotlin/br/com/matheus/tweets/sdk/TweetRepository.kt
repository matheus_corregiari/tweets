package br.com.matheus.tweets.sdk

import br.com.matheus.tweets.sdk.data.remote.apiInstance
import br.com.matheus.tweets.sdk.model.domain.NewTweetVO

object TweetRepository {

    fun getLastTweets() = apiInstance.lastTweets()


    fun addTweet(text: String) = apiInstance.addTweet(NewTweetVO(text))

}