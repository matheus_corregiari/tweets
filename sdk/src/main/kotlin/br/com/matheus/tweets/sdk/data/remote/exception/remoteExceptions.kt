package br.com.matheus.tweets.sdk.data.remote.exception

import java.lang.Exception

open class RemoteException internal constructor(message: String,
                                                val code: Int,
                                                val requestedPath: String) : Exception(message) {
    override fun toString(): String {
        return "code: $code\npath: $requestedPath"
    }
}

class UnauthorizedException internal constructor(message: String, code: Int, requestedPath: String) :
        RemoteException(message, code, requestedPath)

class ServerException internal constructor(message: String, code: Int, requestedPath: String) :
        RemoteException(message, code, requestedPath)

class GatewayTimeoutException internal constructor(message: String, code: Int, requestedPath: String) :
        RemoteException(message, code, requestedPath)

class NotFoundException internal constructor(message: String, code: Int, requestedPath: String) :
        RemoteException(message, code, requestedPath)

class ForbiddenException internal constructor(message: String, code: Int, requestedPath: String) :
        RemoteException(message, code, requestedPath)

class BadRequestException internal constructor(message: String, code: Int, requestedPath: String) :
        RemoteException(message, code, requestedPath)