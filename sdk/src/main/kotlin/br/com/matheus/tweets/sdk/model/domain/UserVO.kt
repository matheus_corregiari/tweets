package br.com.matheus.tweets.sdk.model.domain

import android.os.Parcel
import br.com.matheus.tweets.sdk.extension.KParcelable
import br.com.matheus.tweets.sdk.extension.parcelableCreator
import com.google.gson.annotations.Expose

data class UserVO(
        @Expose val id: Long,
        @Expose val name: String,
        @Expose val screenName: String,
        @Expose val description: String,
        @Expose val profileImageUrl: String,
        @Expose val profileImageUrlHttps: String,
        @Expose val profileBackgroundImageUrl: String,
        @Expose val profileBackgroundImageUrlHttps: String
) : KParcelable {

    companion object {
        val CREATOR = parcelableCreator(::UserVO)
    }

    private constructor(source: Parcel) : this(
            id = source.readLong(),
            name = source.readString(),
            screenName = source.readString(),
            description = source.readString(),
            profileImageUrl = source.readString(),
            profileImageUrlHttps = source.readString(),
            profileBackgroundImageUrl = source.readString(),
            profileBackgroundImageUrlHttps = source.readString()
    )

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeLong(id)
        writeString(name)
        writeString(screenName)
        writeString(description)
        writeString(profileImageUrl)
        writeString(profileImageUrlHttps)
        writeString(profileBackgroundImageUrl)
        writeString(profileBackgroundImageUrlHttps)
    }
}