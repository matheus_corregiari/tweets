package br.com.matheus.tweets.sdk

import br.com.matheus.tweets.sdk.data.remote.apiInstance

object UserRepository {

    private val userLiveData = apiInstance.getUser()

    fun getUser() = userLiveData

}