package br.com.matheus.tweets.sdk

import br.com.matheus.tweets.sdk.data.remote.apiInstance
import br.com.matheus.tweets.sdk.extension.map
import br.com.matheus.tweets.sdk.model.domain.SearchResultVO

object SearchRepository {

    fun searchTweets(term: String) = apiInstance.searchTweets(term).map(SearchResultVO::statuses)

}