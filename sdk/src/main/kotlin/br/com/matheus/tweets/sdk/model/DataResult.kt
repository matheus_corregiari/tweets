package br.com.matheus.tweets.sdk.model

import br.com.matheus.tweets.sdk.model.type.DataResultStatus

data class DataResult<out T>(
        val data: T?,
        var error: Throwable?,
        @get:DataResultStatus
        @DataResultStatus val status: Long
)