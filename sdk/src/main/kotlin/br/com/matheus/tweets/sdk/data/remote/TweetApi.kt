package br.com.matheus.tweets.sdk.data.remote

import br.com.carrefour.sdk.BuildConfig
import br.com.matheus.tweets.sdk.data.ResponseLiveData
import br.com.matheus.tweets.sdk.data.remote.adapter.DateTypeAdapter
import br.com.matheus.tweets.sdk.data.remote.factory.LiveDataCallAdapterFactory
import br.com.matheus.tweets.sdk.data.remote.interceptor.ResponseInterceptor
import br.com.matheus.tweets.sdk.model.DateType
import br.com.matheus.tweets.sdk.model.domain.NewTweetVO
import br.com.matheus.tweets.sdk.model.domain.SearchResultVO
import br.com.matheus.tweets.sdk.model.domain.TweetVO
import br.com.matheus.tweets.sdk.model.domain.UserVO
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import timber.log.Timber

internal interface CarrefourApi {

    @GET("api/user")
    fun getUser(): ResponseLiveData<UserVO>

    @GET("api/statuses/user_timeline")
    fun lastTweets(): ResponseLiveData<List<TweetVO>>

    @GET("api/search/{query}")
    fun searchTweets(@Path("query") term: String): ResponseLiveData<SearchResultVO>

    @POST("api/statuses/update")
    fun addTweet(@Body newTweetVO: NewTweetVO): ResponseLiveData<TweetVO>

}

internal val gson = GsonBuilder()
        .registerTypeAdapter(DateType::class.java, DateTypeAdapter())
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .create()

internal val apiInstance: CarrefourApi = Retrofit.Builder()
        .addConverterFactory(buildGson())
        .client(buildClient())
        .addCallAdapterFactory(LiveDataCallAdapterFactory())
        .baseUrl(BuildConfig.BASE_URL).build().create(CarrefourApi::class.java)

private fun buildClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor { Timber.i(it) }.setLevel(Level.BODY)
    return OkHttpClient.Builder()
            .addInterceptor(logging)
            .addInterceptor(ResponseInterceptor())
            .build()
}

private fun buildGson() = GsonConverterFactory.create(gson)