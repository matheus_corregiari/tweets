package br.com.matheus.tweets.adapter

import android.content.Context
import android.view.View
import br.com.matheus.tweets.base.BaseRecyclerAdapter
import br.com.matheus.tweets.base.ViewBinder

class SimpleAdapter<MODEL, VIEW>(creator: (context: Context) -> VIEW) :
        BaseRecyclerAdapter<MODEL>({ context, _ -> creator.invoke(context) })
        where VIEW : View, VIEW : ViewBinder<MODEL> {

    fun addItemOnTop(model: MODEL) {
        items.add(0, model)
        notifyItemInserted(0)
    }
}