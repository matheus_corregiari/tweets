package br.com.matheus.tweets

import android.app.Application
import timber.log.Timber

class TweetsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}