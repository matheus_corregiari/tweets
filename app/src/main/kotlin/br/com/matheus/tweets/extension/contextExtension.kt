package br.com.matheus.tweets.extension

import android.content.Context
import android.content.Intent
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog
import android.widget.Toast
import br.com.matheus.tweets.base.BaseActivity
import kotlin.reflect.KClass

fun Context.toast(data: Any) = toast(data.toString())

fun Context.toast(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Context.toast(@StringRes msgRes: Int) = Toast.makeText(this, msgRes, Toast.LENGTH_SHORT).show()

fun Context.alert(@StringRes title: Int, @StringRes message: Int) = AlertDialog.Builder(this).setTitle(title).setMessage(message).show()

fun Context.alert(title: String?, message: String?) = AlertDialog.Builder(this).setTitle(title).setMessage(message).show()

fun Context.statusBarHeight(): Int {
    var result = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) result = resources.getDimensionPixelSize(resourceId)
    return result
}

fun <T : BaseActivity> Context.startActivity(zClass: KClass<T>) {
    startActivity(Intent(this, zClass.java))
}

fun <T : BaseActivity> BaseActivity.startActivityForResult(zClass: KClass<T>, requestCode: Int) {
    startActivityForResult(Intent(this, zClass.java), requestCode)
}