package br.com.matheus.tweets.feature.tweet

import br.com.matheus.tweets.base.BaseViewModel
import br.com.matheus.tweets.sdk.TweetRepository

class AddTweetViewModel : BaseViewModel() {

    fun addTweet(text: String) = TweetRepository.addTweet(text)

}