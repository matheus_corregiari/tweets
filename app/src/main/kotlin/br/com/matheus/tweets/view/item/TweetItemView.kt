package br.com.matheus.tweets.view.item

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import br.com.matheus.tweets.R
import br.com.matheus.tweets.base.ViewBinder
import br.com.matheus.tweets.delegate.viewProvider
import br.com.matheus.tweets.extension.loadUrl
import br.com.matheus.tweets.extension.obtain
import br.com.matheus.tweets.sdk.model.domain.TweetVO

class TweetItemView : RelativeLayout, ViewBinder<TweetVO> {

    private val userProfileImg: ImageView by viewProvider(R.id.user_profile_image)
    private val userName: TextView by viewProvider(R.id.user_name_text)
    private val tweetText: TextView by viewProvider(R.id.tweet_text)

    init {
        inflate(context, R.layout.item_tweet_view, this)
    }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, R.attr.tweetItemViewStyle)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        setup(context.obtainStyledAttributes(attrs, R.styleable.TweetItemView, defStyleAttr, defStyleRes))
    }

    override fun bind(model: TweetVO) {
        userProfileImg.loadUrl(model.user.profileImageUrl)
        userName.text = model.user.screenName
        tweetText.text = model.text
    }

    private fun setup(typedArray: TypedArray) = typedArray.obtain {
        // Custom UI based on XML or theme parameters .. hehe
    }

}