package br.com.matheus.tweets.feature.tweet

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.ImageView
import android.widget.TextView
import br.com.matheus.tweets.R
import br.com.matheus.tweets.base.BaseActivity
import br.com.matheus.tweets.delegate.extraProvider
import br.com.matheus.tweets.delegate.viewProvider
import br.com.matheus.tweets.extension.enableBack
import br.com.matheus.tweets.extension.loadUrl
import br.com.matheus.tweets.sdk.model.domain.TweetVO

class TweetActivity : BaseActivity() {

    private val tweet: TweetVO by extraProvider(TWEET_EXTRA)

    private val toolbar: Toolbar by viewProvider(R.id.toolbar)
    private val userProfileImg: ImageView by viewProvider(R.id.user_profile_image)
    private val userName: TextView by viewProvider(R.id.user_name_text)
    private val tweetText: TextView by viewProvider(R.id.tweet_text)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tweet)

        // Setup Toolbar
        setSupportActionBar(toolbar)
        supportActionBar.enableBack()

        userName.text = tweet.user.name
        userProfileImg.loadUrl(tweet.user.profileImageUrl)
        tweetText.text = tweet.text
    }

    companion object {

        private const val TWEET_EXTRA = "ADD_TWEET_RESULT"

        fun start(context: Context, tweetVO: TweetVO) {
            context.startActivity(Intent(context, TweetActivity::class.java).putExtra(TWEET_EXTRA, tweetVO))
        }
    }

}