package br.com.matheus.tweets.feature.tweet

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import br.com.matheus.tweets.R
import br.com.matheus.tweets.base.BaseActivity
import br.com.matheus.tweets.delegate.viewModelProvider
import br.com.matheus.tweets.delegate.viewProvider
import br.com.matheus.tweets.extension.enableBack
import br.com.matheus.tweets.extension.startActivityForResult
import br.com.matheus.tweets.extension.toast

class AddTweetActivity : BaseActivity() {

    private val viewModel by viewModelProvider(AddTweetViewModel::class)

    private val toolbar: Toolbar by viewProvider(R.id.toolbar)
    private val textSizeCounterText: TextView by viewProvider(R.id.tweet_remaining_characters)
    private val tweetInput: EditText by viewProvider(R.id.tweet_input)
    private val sendBtn: Button by viewProvider(R.id.add_tweet_button)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_tweet)

        // Setup Toolbar
        setSupportActionBar(toolbar)
        supportActionBar.enableBack()

        setupCharacterCount()

        sendBtn.setOnClickListener(this::onSendClick)
    }

    private fun setupCharacterCount() {
        tweetInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable) {
                sendBtn.isEnabled = p0.length <= tweetInput.maxEms
                textSizeCounterText.text = "${tweetInput.maxEms - p0.length}"
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
        })
    }

    private fun onSendClick(view: View) {
        val tweetText = tweetInput.text.toString()
        if (tweetText.isEmpty()) view.context.toast(R.string.empty_tweet_message)
        else {
            val liveData = viewModel.addTweet(tweetText)

            // Handle Error
            liveData.observeSingleError(this) {
                view.isEnabled = true
                toast(R.string.error_add_tweet)
            }

            // Handle Loading
            liveData.observeSingleLoading(this) {
                view.isEnabled = !it
                toast(R.string.loading)
            }

            // Handle Success
            liveData.observeSingleData(this) {
                val intent = Intent().putExtra(ADD_TWEET_RESULT, it)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }

    companion object {

        const val ADD_TWEET_REQUEST = 123
        const val ADD_TWEET_RESULT = "ADD_TWEET_RESULT"

        fun start(baseActivity: BaseActivity) {
            baseActivity.startActivityForResult(AddTweetActivity::class, ADD_TWEET_REQUEST)
        }
    }

}