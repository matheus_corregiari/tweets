package br.com.matheus.tweets.view

import android.arch.lifecycle.LifecycleOwner
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import br.com.matheus.tweets.R
import br.com.matheus.tweets.STATE_ERROR
import br.com.matheus.tweets.STATE_LOADING
import br.com.matheus.tweets.STATE_SUCCESS
import br.com.matheus.tweets.delegate.viewProvider
import br.com.matheus.tweets.extension.loadUrl
import br.com.matheus.tweets.extension.obtain
import br.com.matheus.tweets.sdk.data.ResponseLiveData
import br.com.matheus.tweets.sdk.model.domain.UserVO
import br.com.matheus.tweets.statemachine.ViewStateMachine

class UserHeaderView : FrameLayout {

    private val stateMachine = ViewStateMachine()

    private val name: TextView by viewProvider(R.id.name_text)
    private val description: TextView by viewProvider(R.id.description_text)
    private val profileImg: ImageView by viewProvider(R.id.profile_image)
    private val backgroundImg: ImageView by viewProvider(R.id.background_image)

    private val retryBtn: Button by viewProvider(R.id.retry_button)

    private val success: View by viewProvider(R.id.success)
    private val loading: View by viewProvider(R.id.loading)
    private val error: View by viewProvider(R.id.error)

    init {
        inflate(context, R.layout.view_user_header, this)
        setupStateMachine()
    }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, R.attr.userHeaderViewStyle)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        setup(context.obtainStyledAttributes(attrs, R.styleable.UserHeaderView, defStyleAttr, defStyleRes))
    }

    fun observe(lifecycleOwner: LifecycleOwner, liveData: ResponseLiveData<UserVO>) {
        liveData.observeError(lifecycleOwner, this::toErrorState)
        liveData.observeLoading(lifecycleOwner) { if (it) toLoadingState() }
        liveData.observeData(lifecycleOwner, this::toSuccessState)
        setOnRetryClickListener { liveData.invalidate() }
    }

    private fun toLoadingState() = stateMachine.changeState(STATE_LOADING)

    private fun toErrorState(throwable: Throwable) = stateMachine.changeState(STATE_ERROR)

    private fun toSuccessState(user: UserVO) {
        name.text = user.name
        description.text = user.description
        profileImg.loadUrl(user.profileImageUrl)
        backgroundImg.loadUrl(user.profileBackgroundImageUrl)

        stateMachine.changeState(STATE_SUCCESS)
    }

    private fun setOnRetryClickListener(onRetryClick: (View) -> Unit) {
        retryBtn.setOnClickListener(onRetryClick)
    }

    private fun setup(typedArray: TypedArray) = typedArray.obtain {
        // Custom UI based on XML or theme parameters .. hehe
    }

    private fun setupStateMachine() = stateMachine.setup {
        add(STATE_LOADING) {
            visibles(loading)
            gones(success, error)
        }
        add(STATE_SUCCESS) {
            visibles(success)
            gones(loading, error)
        }
        add(STATE_ERROR) {
            visibles(error)
            gones(success, loading)
        }
    }

}