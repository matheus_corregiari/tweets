package br.com.matheus.tweets.feature.search

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.Button
import android.widget.EditText
import br.com.matheus.tweets.R
import br.com.matheus.tweets.base.BaseActivity
import br.com.matheus.tweets.delegate.viewModelProvider
import br.com.matheus.tweets.delegate.viewProvider
import br.com.matheus.tweets.extension.enableBack
import br.com.matheus.tweets.extension.startActivity
import br.com.matheus.tweets.extension.toast
import br.com.matheus.tweets.feature.tweet.TweetActivity
import br.com.matheus.tweets.view.TweetListView

class SearchActivity : BaseActivity() {

    private val viewModel by viewModelProvider(SearchViewModel::class)

    private val toolbar: Toolbar by viewProvider(R.id.toolbar)

    private val inputText: EditText by viewProvider(R.id.search_input)
    private val searchBtn: Button by viewProvider(R.id.search_button)
    private val searchResult: TweetListView by viewProvider(R.id.search_result)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        // Setup Toolbar
        setSupportActionBar(toolbar)
        supportActionBar.enableBack()

        // Setup Last Tweets Section
        searchResult.setOnItemClickListener { TweetActivity.start(this, it) }

        // setup Search
        searchBtn.setOnClickListener {
            val term = inputText.text.toString()
            if (term.isEmpty()) toast(R.string.empty_search_input)
            else searchResult.observe(this, viewModel.search(term))
        }
    }

    companion object {
        fun start(context: Context) {
            context.startActivity(SearchActivity::class)
        }
    }

}