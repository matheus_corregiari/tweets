package br.com.matheus.tweets.feature.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import br.com.matheus.tweets.R
import br.com.matheus.tweets.base.BaseActivity
import br.com.matheus.tweets.delegate.viewModelProvider
import br.com.matheus.tweets.delegate.viewProvider
import br.com.matheus.tweets.feature.search.SearchActivity
import br.com.matheus.tweets.feature.tweet.AddTweetActivity
import br.com.matheus.tweets.feature.tweet.TweetActivity
import br.com.matheus.tweets.sdk.model.domain.TweetVO
import br.com.matheus.tweets.view.TweetListView
import br.com.matheus.tweets.view.UserHeaderView

class UserActivity : BaseActivity() {

    private val viewModel by viewModelProvider(UserViewModel::class)

    private val userHeader: UserHeaderView by viewProvider(R.id.user_header)
    private val lastTweetsView: TweetListView by viewProvider(R.id.last_tweets)
    private val addTweetBtn: Button by viewProvider(R.id.add_tweet_button)
    private val searchBtn: Button by viewProvider(R.id.search_button)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        // Setup User Section
        userHeader.observe(this, viewModel.userLiveData)

        // Setup Last Tweets Section
        lastTweetsView.observe(this, viewModel.lastTweetsLiveData)
        lastTweetsView.setOnItemClickListener { TweetActivity.start(this, it) }

        // Setup Add Tweet
        addTweetBtn.setOnClickListener { AddTweetActivity.start(this) }

        // setup Search
        searchBtn.setOnClickListener { SearchActivity.start(it.context) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AddTweetActivity.ADD_TWEET_REQUEST && resultCode == Activity.RESULT_OK) {
            val newTweet: TweetVO = data!!.getParcelableExtra(AddTweetActivity.ADD_TWEET_RESULT)
            lastTweetsView.addTweet(newTweet)
        }
    }

}