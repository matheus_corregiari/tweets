package br.com.matheus.tweets.feature.search

import br.com.matheus.tweets.base.BaseViewModel
import br.com.matheus.tweets.sdk.SearchRepository

class SearchViewModel : BaseViewModel() {

    fun search(term: String) = SearchRepository.searchTweets(term)
}
