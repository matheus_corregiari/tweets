package br.com.matheus.tweets.feature.user

import br.com.matheus.tweets.base.BaseViewModel
import br.com.matheus.tweets.sdk.TweetRepository
import br.com.matheus.tweets.sdk.UserRepository

class UserViewModel : BaseViewModel() {

    val userLiveData = UserRepository.getUser()

    val lastTweetsLiveData = TweetRepository.getLastTweets()

}