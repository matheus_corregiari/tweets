package br.com.matheus.tweets.view

import android.arch.lifecycle.LifecycleOwner
import android.content.Context
import android.content.res.TypedArray
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import br.com.matheus.tweets.*
import br.com.matheus.tweets.adapter.SimpleAdapter
import br.com.matheus.tweets.delegate.viewProvider
import br.com.matheus.tweets.extension.obtain
import br.com.matheus.tweets.sdk.data.ResponseLiveData
import br.com.matheus.tweets.sdk.model.domain.TweetVO
import br.com.matheus.tweets.statemachine.ViewStateMachine
import br.com.matheus.tweets.view.item.TweetItemView

class TweetListView : FrameLayout {

    private val stateMachine = ViewStateMachine()
    private val adapter = SimpleAdapter(::TweetItemView)

    private val retryBtn: Button by viewProvider(R.id.retry_button)

    private val success: RecyclerView by viewProvider(R.id.success)
    private val loading: View by viewProvider(R.id.loading)
    private val error: View by viewProvider(R.id.error)
    private val empty: View by viewProvider(R.id.empty)

    init {
        inflate(context, R.layout.view_tweet_list, this)
        success.layoutManager = LinearLayoutManager(context)
        success.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        success.adapter = adapter
        setupStateMachine()
    }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, R.attr.tweetListViewStyle)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        setup(context.obtainStyledAttributes(attrs, R.styleable.TweetListView, defStyleAttr, defStyleRes))
    }

    fun observe(lifecycleOwner: LifecycleOwner, liveData: ResponseLiveData<List<TweetVO>>) {
        liveData.observeError(lifecycleOwner, this::toErrorState)
        liveData.observeLoading(lifecycleOwner) { if (it) toLoadingState() }
        liveData.observeData(lifecycleOwner) {
            if (it.isEmpty()) this.toEmptyState()
            else this.toSuccessState(it)
        }
        setOnRetryClickListener { liveData.invalidate() }
    }

    fun setOnItemClickListener(onItemClick: (TweetVO) -> Unit) {
        adapter.onItemClick = onItemClick
    }

    fun addTweet(tweetVO: TweetVO) {
        adapter.addItemOnTop(tweetVO)
        success.smoothScrollToPosition(0)
    }

    private fun toLoadingState() = stateMachine.changeState(STATE_LOADING)

    private fun toErrorState(throwable: Throwable) = stateMachine.changeState(STATE_ERROR)

    private fun toSuccessState(lastTweets: List<TweetVO>) {
        adapter.setList(lastTweets)
        stateMachine.changeState(STATE_SUCCESS)
    }

    private fun toEmptyState() {
        stateMachine.changeState(STATE_EMPTY)
    }

    private fun setOnRetryClickListener(onRetryClick: (View) -> Unit) {
        retryBtn.setOnClickListener(onRetryClick)
    }

    private fun setup(typedArray: TypedArray) = typedArray.obtain {
        // Custom UI based on XML or theme parameters .. hehe
    }

    private fun setupStateMachine() = stateMachine.setup {
        add(STATE_LOADING) {
            visibles(loading)
            gones(success, error, empty)
        }
        add(STATE_SUCCESS) {
            visibles(success)
            gones(loading, error, empty)
        }
        add(STATE_EMPTY) {
            visibles(empty)
            gones(loading, error, success)
        }
        add(STATE_ERROR) {
            visibles(error)
            gones(success, loading, empty)
        }
    }

}